<?php

class DB {
    private $dbHost = "localhost";
    private $dbUser = "root";
    private $dbPass = "";
    private $dbName = "restful_api";

    public function connect(){
        $mysql_info = "mysql:host=$this->dbHost;dbname=$this->dbName;charset=utf8";
        $connection = new PDO($mysql_info,$this->dbUser,$this->dbPass);

        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $connection;
    }
}