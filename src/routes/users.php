<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


$app = new \Slim\App;

//tüm kulllanıcıları getir
$app->get('/users', function (Request $request, Response $response) {

   $db = new DB();

    try {
        $db = $db->connect();
        $users = $db->query("SELECT * FROM users WHERE deleted=0")->fetchAll(PDO::FETCH_OBJ);

        if(!empty($users)){
            return $response
                    ->withStatus(200)
                    ->withHeader("Content-Type",'application/json')
                    ->withJson($users);
        }else{
            return $response
                    ->withStatus(500)
                    ->withHeader("Content-Type",'application/json')
                    ->withJson( array(
                        "error" => array(
                            "text" => "Data bulunamadı!!"
                        )
                    ));
        }

    }catch (PDOException $exception){
        return $response->withJson(
            array(
                "error" => array(
                    "text" => $exception->getMessage(),
                    "code" => $exception->getCode()
                )
            )
        );
    }
    $db = null;

});

//idye göre kulllanıcı getir
$app->get('/user/{id}', function (Request $request, Response $response) {

    $id = $request->getAttribute("id");
    $db = new DB();

    try {
        $db = $db->connect();
        $query = "SELECT * FROM users WHERE id=:id AND deleted=0";
        $prepare = $db->prepare($query);
        $prepare->bindParam("id",$id);
        $prepare->execute();
        $user = $prepare->fetchAll(PDO::FETCH_OBJ);

        if(!empty($user)){
            return $response
                ->withStatus(200)
                ->withHeader("Content-Type",'application/json')
                ->withJson($user);
        }else{
            return $response
                ->withStatus(500)
                ->withHeader("Content-Type",'application/json')
                ->withJson( array(
                    "error" => array(
                        "text" => "Kullanıcı bulunamadı!!"
                    )
                ));
        }

    }catch (PDOException $exception){
        return $response->withJson(
            array(
                "error" => array(
                    "text" => $exception->getMessage(),
                    "code" => $exception->getCode()
                )
            )
        );
    }
    $db = null;

});

//kullanıcı ekleme
$app->post('/user-add', function (Request $request, Response $response) {

    $name = $request->getParam("name");
    $surname = $request->getParam("surname");
    $email = $request->getParam("email");
    $password = $request->getParam("password");
    $token = $request->getParam("token");
    $db = new DB();

    try {
        $db = $db->connect();
        $query = "INSERT INTO users (name,surname,email,password,token) VALUES (:name,:surname,:email,:password,:token)";
        $prepare = $db->prepare($query);
        $prepare->bindParam("name",$name);
        $prepare->bindParam("surname",$surname);
        $prepare->bindParam("email",$email);
        $prepare->bindParam("password",$password);
        $prepare->bindParam("token",$token);

        $result = $prepare->execute();

        if($result){
            return $response
                ->withStatus(200)
                ->withHeader("Content-Type",'application/json')
                ->withJson(array(
                    "sonuç" => "Kullanıcı başarıyla oluşturuldu!!"
                ));
        }else{
            return $response
                ->withStatus(500)
                ->withHeader("Content-Type",'application/json')
                ->withJson(array(
                    "sonuç" => "Kullanıcı oluşturulurken bir hata oluştu!!"
                ));
        }

    }catch (PDOException $exception){
        return $response->withJson(
            array(
                "error" => array(
                    "text" => $exception->getMessage(),
                    "code" => $exception->getCode()
                )
            )
        );
    }
    $db = null;

});

//idye göre kulllanıcı güncelle
$app->put('/user-update/{id}', function (Request $request, Response $response) {

    $id = $request->getAttribute("id");

    if ($id){
        $name = $request->getParam("name");
        $surname = $request->getParam("surname");
        $email = $request->getParam("email");
        $password = $request->getParam("password");
        $token = $request->getParam("token");

        $db = new DB();

        try {
            $db = $db->connect();
            $query = "UPDATE users SET name = :name, surname=:surname, email=:email, password=:password, token=:token WHERE id=:id";
            $prepare = $db->prepare($query);
            $prepare->bindParam("id",$id);
            $prepare->bindParam("name",$name);
            $prepare->bindParam("surname",$surname);
            $prepare->bindParam("email",$email);
            $prepare->bindParam("password",$password);
            $prepare->bindParam("token",$token);

            $result = $prepare->execute();

            if($result){
                return $response
                    ->withStatus(200)
                    ->withHeader("Content-Type",'application/json')
                    ->withJson(array(
                        "sonuç" => "Kullanıcı başarıyla güncellendi!!"
                    ));
            }else{
                return $response
                    ->withStatus(500)
                    ->withHeader("Content-Type",'application/json')
                    ->withJson(array(
                        "sonuç" => "Kullanıcı güncellerken bir hata oluştu!!"
                    ));
            }

        }catch (PDOException $exception){
            return $response->withJson(
                array(
                    "error" => array(
                        "text" => $exception->getMessage(),
                        "code" => $exception->getCode()
                    )
                )
            );
        }
        $db = null;

    }else{
        return $response
            ->withStatus(500)
            ->withHeader("Content-Type",'application/json')
            ->withJson( array(
                "error" => array(
                    "text" => "Güncellemek istediğiniz kullanıcı bulunamadı!!"
                )
            ));
    }


});

//idye göre kulllanıcı (soft)silme
$app->put('/user-delete/{id}', function (Request $request, Response $response) {

    $id = $request->getAttribute("id");

    if ($id){
        $db = new DB();

        try {
            $db = $db->connect();
            $query = "UPDATE users SET deleted = 1 WHERE id=:id";
            $prepare = $db->prepare($query);
            $prepare->bindParam(id,$id);

            $result = $prepare->execute();

            if($result){
                return $response
                    ->withStatus(200)
                    ->withHeader("Content-Type",'application/json')
                    ->withJson(array(
                        "sonuç" => "Kullanıcı başarıyla silindi!!"
                    ));
            }else{
                return $response
                    ->withStatus(500)
                    ->withHeader("Content-Type",'application/json')
                    ->withJson(array(
                        "sonuç" => "Kullanıcı silerken bir hata oluştu!!"
                    ));
            }

        }catch (PDOException $exception){
            return $response->withJson(
                array(
                    "error" => array(
                        "text" => $exception->getMessage(),
                        "code" => $exception->getCode()
                    )
                )
            );
        }
        $db = null;

    }else{
        return $response
            ->withStatus(500)
            ->withHeader("Content-Type",'application/json')
            ->withJson( array(
                "error" => array(
                    "text" => "Silmek istediğiniz kullanıcı bulunamadı!!"
                )
            ));
    }


});