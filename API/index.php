<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/config/database.php';

$app = new \Slim\App;

/*users import edelim*/
require "../src/routes/users.php";

$app->run();
